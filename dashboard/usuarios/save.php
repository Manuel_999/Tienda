<?php
require("../page/designe.php");

if (empty($_GET['id'])) {
  Page::header("Agregar usuario");
  $id          = null;
  $nombres     = null;
  $apellidos   = null;
  $usuario     = null;
  $clave       = null;
  $tipo        = null;
  $imagen      = null;
  $desabilitar = null;
} else {
  Page::header("Modificar usuario");
  $id          = base64_decode($_GET['id']);
  $sql         = "SELECT * FROM usuarios, tipo_usuario WHERE tipo_usuario.id_tipo_usuario = usuarios.id_tipo_usuario and id_usuario = ?";
  $params      = array(
    $id
  );
  $data        = Database::getRow($sql, $params);
  $nombres     = $data['nombres'];
  $apellidos   = $data['apellidos'];
  $usuario     = $data['usuario'];
  $clave       = $data['clave'];
  $tipo        = $data['tipo_usuario'];
  $imagen      = $data['foto'];
  $desabilitar = "disabled";
}

#Funcion que sirve para validar la contraseña, donde se validara longitud de la clave, si tiene mayusculas y minusculas.
function validar_clave($clave, &$error_clave)
{
  if (strlen($clave) < 8) {
    $error_clave = "La clave debe tener al menos 8 caracteres";
    return false;
  }
  if (strlen($clave) > 16) {
    $error_clave = "La clave no puede tener más de 16 caracteres";
    return false;
  }
  if (!preg_match('`[a-z]`', $clave)) {
    $error_clave = "La clave debe tener al menos una letra minúscula";
    return false;
  }
  if (!preg_match('`[A-Z]`', $clave)) {
    $error_clave = "La clave debe tener al menos una letra mayúscula";
    return false;
  }
  if (!preg_match('`[0-9]`', $clave)) {
    $error_clave = "La clave debe tener al menos un caracter numérico";
    return false;
  }
  $error_clave = "";
  return true;
}


if (!empty($_POST)) {
  $nombres   = $_POST['nombre'];
  $apellidos = $_POST['apellido'];
  $usuario   = $_POST['usuario'];
  $clave     = $_POST['clave'];
  $clave2    = $_POST['clave2'];
  $tipo      = $_POST['tipo'];
  $archivo   = $_FILES['imagen'];
  
  #Expresion regular que sirve para validar nombres y apellidos que en las iniciales de cada nombre tenga espacios y no tenga numeros.
  $nombre = "/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/";
  try {
    #Se valida los espacios vacios de cada campo.
    if ($nombres != "") {
      #Se valida aqui con el campo nombre y la expresion regular anteriormente hecha.
      if (preg_match($nombre, $_POST['nombre'])) {
        if ($apellidos != "") {
          #Se valida aqui con el campo apellido y la expresion regular anteriormente hecha.
          if (preg_match($nombre, $_POST['apellido'])) {
            if ($usuario != "") {
              #Se valida el correo.
              if (filter_var($_POST['usuario'], FILTER_VALIDATE_EMAIL)) {
                if ($archivo['name'] != null) {
                  $base64 = Validator::validateImage($archivo);
                  if ($base64 != false) {
                    $imagen = $base64;
                  } else {
                    throw new Exception("Ocurrió un problema con la imagen");
                  }
                } else {
                  #imagen predefinida
                  if ($imagen == null) {
                    $imagen = Validator::imagen_usuario();
                  }
                }
                if ($id == null) {
                  
                  $error_encontrado = "";
                  #Se valida la clave ingresada y se manda a la funcion anteriormente hecha, para verificar cada uno de los requisitos.
                  if (validar_clave($_POST['clave'], $error_encontrado)) {
                    if ($clave != "") {
                      #Se valida que las dos claves sean iguales.
                      if ($clave == $clave2) {
                        #Se encripta la clave con paswrod hash
                        $clave3 = password_hash($clave, PASSWORD_DEFAULT);
                        $sql    = "INSERT INTO usuarios(nombres, apellidos, usuario, clave, id_tipo_usuario, foto) VALUES (?,?,?,?,?,?)";
                        $params = array(
                          $nombres,
                          $apellidos,
                          $usuario,
                          $clave3,
                          $tipo,
                          $imagen
                        );
                      } else {
                        throw new Exception("Debe coincidir las dos claves!");
                      }
                    } else {
                      throw new Exception("Debe ingresar la clave!");
                    }
                  } else {
                    throw new Exception($error_encontrado);
                  }
                  
                  
                }
                if ($id != null) {
                  $sql    = "UPDATE usuarios SET nombres=?,apellidos=?,usuario=?,id_tipo_usuario=?,foto=? WHERE id_usuario = ?";
                  $params = array(
                    $nombres,
                    $apellidos,
                    $usuario,
                    $tipo,
                    $imagen,
                    $id
                  );
                }
                Database::executeRow($sql, $params);
                header("location: index.php");
              } else {
                throw new Exception("Verifique el usuario!");
              }
              
            } else {
              throw new Exception("Debe ingresar un usuario!");
            }
          } else {
            throw new Exception("Verifique el apellido!");
          }
        } else {
          throw new Exception("Debe los apellidos!");
        }
      } else {
        throw new Exception("Verifique el nombre!");
      }
    } else {
      throw new Exception("Debe ingresar los nombres!");
    }
  }
  catch (Exception $error) {
    print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>" . $error->getMessage() . "</div>");
  }
}


?>

<h2 class="center-align">Usuario</h2>
<hr id="linea">

<form class="container" method="post" enctype='multipart/form-data'>
    <div class='file-field input-field col s12 m6'>
            <div class='btn waves-effect green darken-2'>
                <span><i class='material-icons'>image</i></span>
                <input type='file' name='imagen' <?php
print(($imagen == null) ?: "");
?>/>
            </div>
            <div class='file-path-wrapper'>
                <input class='file-path validate' type='text' placeholder='Seleccione una imagen'/>
            </div>
      </div>
      <div class="row">
        <div class="input-field col s12 l6">
          <i class="material-icons prefix">perm_identity</i>
          <input id="icon_prefix" type="text" autocomplete="off" class="validate" name="nombre" value="<?php
print($nombres);
?>">
          <label for="icon_prefix">Nombres</label>
        </div>
        <div class="input-field col s12 l6">
          <i class="material-icons prefix">perm_identity</i>
          <input id="icon_prefix" type="text" autocomplete="off" class="validate" name="apellido" value="<?php
print($apellidos);
?>">
          <label for="icon_prefix">Apellidos</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12 l12">
          <i class="material-icons prefix">assignment_ind</i>
          <input id="icon_prefix" type="email" class="validate" name="usuario" value="<?php
print($usuario);
?>" autocomplete="off">
          <label for="icon_prefix">Usuario</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12 l6">
          <i class="material-icons prefix">security</i>
          <input id="icon_prefix" type="password" class="validate" name="clave" autocomplete="off" <?php
print($desabilitar);
?>>
          <label for="icon_prefix">Contraseña</label>
        </div>
        <div class="input-field col s12 l6">
          <i class="material-icons prefix">security</i>
          <input id="icon_prefix" type="password" class="validate" name="clave2" autocomplete="off" <?php
print($desabilitar);
?>>
          <label for="icon_prefix">Confirmar contraseña</label>
        </div>
      </div>
      <div class="row">
          <div class='input-field col s12 m12'>
            <?php
$sql = "SELECT id_tipo_usuario, tipo_usuario FROM tipo_usuario";
Page::setCombo("Tipo de usuario", "tipo", $tipo, $sql);
?>
       </div>
      </div>

          <div align="center">
              <a href='index.php' class='btn grey darken-4'><i class='material-icons right'>cancel</i>Cancelar</a>
                <br class="hide-on-med-and-up">
                <br class="hide-on-med-and-up">
              <button type='submit' name="action" class='btn red darken-4'><i class='material-icons right'>send</i>Guardar</button>
          </div>
      <div class="section"></div>
</form>

<?php
Page::footer();
?>