<?php
require("../page/designe.php");
Page::header("Usuarios");
?>
<form method='post'>
    <div class='row'>
        <div class='input-field col s6 m4'>
            <i class='material-icons prefix'>search</i>
            <input id='buscar' type='text' name='buscar'/>
            <label for='buscar'>Buscar</label>
        </div>
        <div class='input-field col s6 m4'>
            <button type='submit' class='btn waves-effect green darken-3'><i class='material-icons'>check_circle</i></button>     
        </div>
        <div class='input-field col s12 m4'>
            <a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
        </div>
    </div>
</form>

<?php

if (!empty($_POST)) {
  $search = trim($_POST['buscar']);
  $sql    = "SELECT * FROM usuarios, tipo_usuario WHERE tipo_usuario.id_tipo_usuario = usuarios.id_tipo_usuario AND usuario LIKE ? ORDER BY usuario";
  $params = array(
    "%$search%"
  );
} else {
  $sql    = "SELECT * FROM usuarios, tipo_usuario WHERE tipo_usuario.id_tipo_usuario = usuarios.id_tipo_usuario ORDER BY usuario";
  $params = null;
}
$data = Database::getRows($sql, $params);
if ($data != null) {
?>
<table class='striped'>
    <thead>
        <tr>
            <th>IMAGEN</th>
            <th>NOMBRE</th>
            <th>APELLIDO</th>
            <th>USUARIO</th>
            <th>TIPO</th>
            <th>ACCIÓN</th>
        </tr>
    </thead>
    <tbody>

<?php
  foreach ($data as $row) {
    print("
            <tr>
                <td><img src='data:image/*;base64," . $row['foto'] . "' class='materialboxed' width='100' height='100'></td>
                <td>" . $row['nombres'] . "</td>
                <td>" . $row['apellidos'] . "</td>
                <td>" . $row['usuario'] . "</td>
                <td>" . $row['tipo_usuario'] . "</td>
        ");
    if ($_SESSION['id'] != $row['id_usuario']) {
      print("
            <td>
                    <a href='save.php?id=" . base64_encode($row['id_usuario']) . "' class='blue-text'><i class='material-icons'>mode_edit</i></a>
                    <a href='delete.php?id=" . base64_encode($row['id_usuario']) . "' class='red-text'><i class='material-icons'>delete</i></a>
                </td>");
    } else {
      print("
            <td>
                    <a href='save.php?id=" . base64_encode($row['id_usuario']) . "' class='blue-text'><i class='material-icons'>mode_edit</i></a>
                </td>");
    }
    print("
            </tr>
        ");
  }
  print("
        </tbody>
    </table>
    ");
} //Fin de if que comprueba la existencia de registros.
else {
  print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>No hay registros!</div>");
}
Page::footer();
?>
<?php
include("../../lib/footer.php");
?>