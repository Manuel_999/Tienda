<?php
require("../page/designe.php");

if (empty($_GET['id'])) {
  Page::header("Agregar accesorio");
  $id             = null;
  $titulo         = null;
  $descripcion    = null;
  $estado_visible = null;
  $tipo           = null;
  $precio         = null;
  $imagen         = null;
  $existencia     = null;
} else {
  Page::header("Modificar accesorio");
  $id             = base64_decode($_GET['id']);
  $sql            = "SELECT * FROM accesorios WHERE id_accesorios = ?";
  $params         = array(
    $id
  );
  $data           = Database::getRow($sql, $params);
  $titulo         = $data['titulo'];
  $descripcion    = $data['descripcion'];
  $estado_visible = $data['estado_visible'];
  $tipo           = $data['id_tipo_accesorio'];
  $precio         = $data['precio'];
  $existencia     = $data['existencias'];
  $imagen         = $data['foto'];
}

if (!empty($_POST)) {
  $titulo         = $_POST['titulo'];
  $descripcion    = $_POST['descripcion'];
  $estado_visible = $_POST['estado'];
  $tipo           = $_POST['tipo'];
  $precio         = $_POST['precio'];
  $existencia     = $_POST['existencia'];
  $archivo        = $_FILES['imagen'];
  
  try {
    #Se valida que los campos no esten vacios.
    if ($titulo != "") {
      if ($descripcion != "") {
        #Sa valida que el campo existencia tenga un valor entero y no una letra
        if (filter_var($existencia, FILTER_VALIDATE_INT)) {
          #Se valida que el precio y la existencia sea mayor a 0
          if ($precio > 0 and $existencia >= 0) {
            if ($archivo['name'] != null) {
              $base64 = Validator::validateImage($archivo);
              if ($base64 != false) {
                $imagen = $base64;
              } else {
                throw new Exception("Ocurrió un problema con la imagen");
              }
            } else {
              #imagen predefinida
              if ($imagen == null) {
                $imagen = Validator::imagen_accesorio();
              }
            }
            
            if ($id == null) {
              $sql    = "INSERT INTO accesorios(titulo, descripcion, precio, id_tipo_accesorio, estado_visible, existencias, foto) VALUES (?,?,?,?,?,?,?)";
              $params = array(
                $titulo,
                $descripcion,
                $precio,
                $tipo,
                $estado_visible,
                $existencia,
                $imagen
              );
            } else {
              $sql    = "UPDATE accesorios SET titulo=?,id_tipo_accesorio=?,descripcion=?,precio=?,estado_visible=?, existencias=?, foto=? WHERE id_accesorios = ?";
              $params = array(
                $titulo,
                $tipo,
                $descripcion,
                $precio,
                $estado_visible,
                $existencia,
                $imagen,
                $id
              );
            }
            Database::executeRow($sql, $params);
            header("location: index.php");
          } else {
            throw new Exception("El precio y/o existencia debe ser mayor que 0.00");
          }
        } else {
          throw new Exception("No se aceptan letras en existencia!");
        }
      } else {
        throw new Exception("Debe ingresar el precio");
      }
    } else {
      throw new Exception("Debe ingresar un titulo");
    }
  }
  catch (Exception $error) {
    print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>" . $error->getMessage() . "</div>");
  }
}


?>

<h2 class="center-align">Accesorio</h2>
<hr id="linea">

<form class="container" method="post" enctype='multipart/form-data'>
      <div class='file-field input-field col s12 m6'>
            <div class='btn waves-effect green darken-2'>
                <span><i class='material-icons'>image</i></span>
                <input type='file' name='imagen' <?php
print(($imagen == null) ?: "");
?>/>
            </div>
            <div class='file-path-wrapper'>
                <input class='file-path validate' type='text' placeholder='Seleccione una imagen'/>
            </div>
      </div>
      <div class="row">
        <div class="input-field col s12 l12">
          <i class="material-icons prefix">done</i>
          <input id="icon_prefix" type="text" autocomplete="off" class="validate" name="titulo" value="<?php
print($titulo);
?>">
          <label for="icon_prefix">Titulo</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <i class="material-icons prefix">mode_edit</i>
          <textarea id="icon_prefix2" class="materialize-textarea" class="validate" name="descripcion"><?php
print($descripcion);
?></textarea>
          <label for="icon_prefix2">Descripción</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12 l6">
          <i class="material-icons prefix">trending_up</i>
          <input id="icon_prefix" type="text" class="validate" name="precio" value="<?php
print($precio);
?>" autocomplete="off">
          <label for="icon_prefix">Precio</label>
        </div>
        <div class="input-field col s12 l6">
          <i class="material-icons prefix">grade</i>
          <input id="icon_prefix" type="text" class="validate" name="existencia" value="<?php
print($existencia);
?>" autocomplete="off">
          <label for="icon_prefix">Existencia</label>
        </div>
      </div>
      <div class="row">
          <div class="input-field col s12 m6">
              <select name="estado" required>
                <option value="" disabled selected>Seleccione una opción</option>
                <?php
if ($estado_visible == 2) {
  print("<option value='2' selected>Visible</option>");
} else {
  print("<option value='2'>Visible</option>");
}
if ($estado_visible == 1) {
  print("<option value='1' selected>Invisible</option>");
} else {
  print("<option value='1'>Invisible</option>");
}
?>
             </select>
              <label>Estado Visible:</label>
          </div>
          <div class='input-field col s12 m6'>
            <?php
$sql = "SELECT id_tipo_accesorio, tipo_accesorio FROM tipo_accesorio";
Page::setCombo("Tipo de accesorio", "tipo", $tipo, $sql);
?>
       </div>
      </div>

          <div align="center">
              <a href='index.php' class='btn grey darken-4'><i class='material-icons right'>cancel</i>Cancelar</a>
                <br class="hide-on-med-and-up">
                <br class="hide-on-med-and-up">
              <button type='submit' name="action" class='btn red darken-4'><i class='material-icons right'>send</i>Guardar</button>
          </div>
          <div class="section"></div>
</form>

<?php
Page::footer();
?>

<?php
include("../../ lib/footer.php");
?>