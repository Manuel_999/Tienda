<?php
require("../page/designe.php"); 
Page::header("Accesorios");
?>

         
   
<form method='post'>
<div class='row'>
<div class='input-field col s6 m4'>
    <i class='material-icons prefix'>search</i>
        <input id='buscar' type='text' name='buscar'/>
    <label for='buscar'>Buscar</label>
</div>
    <div class='input-field col s6 m4'>
        <button type='submit' class='btn waves-effect green darken-3'><i class='material-icons'>check_circle</i></button>     
    </div>
        <div class='input-field col s12 m4'>
        <a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
        </div>
    </div>
</form>

<?php
$params = null;
if (!empty($_POST)) {
    $search = trim($_POST['buscar']);
    //$search = "\"%".$search."%\"";
    $sql    = "SELECT * FROM accesorios, tipo_accesorio WHERE tipo_accesorio.id_tipo_accesorio = accesorios.id_tipo_accesorio AND titulo LIKE ? ORDER BY titulo";
    $params = array(
        "%$search%"
    );
} else {
    $sql    = "SELECT * FROM accesorios, tipo_accesorio WHERE tipo_accesorio.id_tipo_accesorio = accesorios.id_tipo_accesorio ORDER BY titulo";
    $params = null;
}
//print_r($params);
//echo $sql;
$data = Database::getRows($sql, $params);
if ($data != null) {
?>
<table class='striped'>
    <thead>
        <tr>
            <th>IMAGEN</th>
            <th>NOMBRE</th>
            <th>PRECIO ($)</th>
            <th>CATEGORÍA</th>
            <th>ESTADO</th>
            <th>EXISTENCIAS</th>
            <th>ACCIÓN</th>
        </tr>
    </thead>
    <tbody>

<?php

// paginacion
	// Se obtiene el numero de registros que hay en la BD
	$sql_product = 'SELECT count(*) FROM accesorios';
	$total_productos = Database::getRow($sql_product,null);
	$total_productos1 = array_pop($total_productos);

	//Se indica el numero se registros por pagina
	$records_per_page = 5;

	//Se llama a la libreria y se crea como objeto para llamar sus funciones
	$pagination = new Zebra_Pagination();

	//Se indica el posicionamiento del elemento paginacion
	$pagination->navigation_position(isset($_GET['navigation_position']) && in_array($_GET['navigation_position'], array('left', 'right')) ? $_GET['navigation_position'] : 'outside');
	
	//Se comprueba el posicinamiento
	if (isset($_GET['direction']) && $_GET['direction'] == 'reversed') $pagination->reverse(true);
	
	//Se le pasa el parameto a la funcion en el cual es el total de registros
	$pagination->records($total_productos1);

	//Se le pasa el parametro de cuantos registros iran por pagina
	$pagination->records_per_page($records_per_page);

	//Se crea la consulta de la cual en el foreach se obtendran los datos
	$productos ='SELECT * FROM accesorios, tipo_accesorio WHERE tipo_accesorio.id_tipo_accesorio = accesorios.id_tipo_accesorio LIMIT '. (($pagination->get_page() - 1) * $records_per_page). ',' . $records_per_page;
	$t_pro = Database::getRows($productos,null);


    foreach ($t_pro as $row) {
        print("
        <tr>
            <td><img src='data:image/*;base64," . $row['foto'] . "' class='materialboxed' width='100' height='100'></td>
            <td>" . $row['titulo'] . "</td>
            <td>" . $row['precio'] . "</td>
            <td>" . $row['tipo_accesorio'] . "</td>
            <td>
    ");
        if ($row['estado_visible'] == 2) {
            print("<i class='material-icons'>visibility</i>");
        } else {
            print("<i class='material-icons'>visibility_off</i>");
        }
        print("
        </td>
        <td>" . $row['existencias'] . "</td>
        <td>
            <a href='save.php?id=" . base64_encode($row['id_accesorios']) . "' class='blue-text'><i class='material-icons'>mode_edit</i></a>
            <a href='delete.php?id=" . base64_encode($row['id_accesorios']) . "' class='red-text'><i class='material-icons'>delete</i></a>
        </td>
        </tr>
    ");
    }
    print("
    </tbody>
    </table>
");
  
	//Va mostrando las paginacion que se va creando
	$pagination->render();

} //Fin de if que comprueba la existencia de registros.
else {
    print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>No hay registros!</div>");
}

 
Page::footer();
?>
<?php
include("../../lib/footer.php");
?>