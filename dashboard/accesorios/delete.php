<?php
require("../page/designe.php");
page::header("Eliminar accesorio");

//Esto valida que tiene que existir un id en la URL para poder eliminar.
if (!empty($_GET['id'])) {
    $id = base64_decode($_GET['id']);
} else {
    header("location: index.php");
}


if (!empty($_POST)) {
    //Se valida que tiene que existir un id para poder eliminar
    if (!empty($_GET['id'])) {
        $id = base64_decode($_GET['id']);
        
        
        
        
        function mthAgregar($id)
        {
            $sql    = "DELETE from accesorios WHERE id_accesorios = ?";
            $params = array(
                $id
            );
            Database::executeRow($sql, $params);
            header("location: index.php");
        }
        mthAgregar($id);
    }
}
?>



<!----------------------------------------------------------------Aqui empieza el formulario------------------------------------>

<div class="container">
 <div class="row" align="center">
 <br>
    <h2 class="center-align">Eliminar Accesorio</h2>
        <p id="eliminarcentro">¿Desea eliminar este accesorio?</p>
        <br>
            <form method='post' class='row'>
            <button  type='submit' name="action" class='btn red darken-4'><i class='material-icons right'>done</i>Si</button>
            <br class="hide-on-med-and-up">
            <br class="hide-on-med-and-up">
            <a href='index.php' class='btn black'><i class='material-icons right'>cancel</i>No</a>
         <br>
        <br>
    <br>
 </form>                           
</div>
</div>

<?php
page::footer();
?>
       </tbody>
    </table>
    ");
}
 //Fin de if que comprueba la existencia de registros.
else
{
    print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>No hay registros!</div>");
}

<?php
include("../../lib/footer.php");
?>