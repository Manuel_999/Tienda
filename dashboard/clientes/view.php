<?php
require("../page/designe.php");
Page::header("Cliente");

$id         = base64_decode($_GET['id']);
$sql        = "SELECT * FROM clientes WHERE id_cliente = ?";
$params     = array(
  $id
);
$data       = Database::getRow($sql, $params);
$nombres    = $data['nombres'];
$apellidos  = $data['apellidos'];
$dui        = $data['dui'];
$nick       = $data['nickname'];
$correo     = $data['correo_electronico'];
$residencia = $data['residencia'];

?>
 


        

<form class="container" method="post" enctype='multipart/form-data'>
    <div class="row">
        <h2 class="center-align">Cliente</h2>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
              <i class='material-icons prefix'>person</i>
              <input id='nombres' type='text' name='nombre' class='validate' disabled="" value="<?php
print($nombres);
?>">
              <label for='nombres'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellidos' type='text' name='apellido' class='validate' disabled="" value="<?php
print($apellidos);
?>">
            <label for='apellidos'>Apellidos</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>assignment_ind</i>
            <input id='apellidos' type='text' name='dui' class='validate' disabled="" value="<?php
print($dui);
?>">
            <label for='apellidos'>DUI</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>recent_actors</i>
            <input id='apellidos' type='text' name='nick' class='validate' disabled="" value="<?php
print($nick);
?>">
            <label for='apellidos'>NickName</label>
        </div>
        <div class='input-field col s12 m12'>
            <i class='material-icons prefix'>email</i>
            <input id='correo' type='email' name='correo' class='validate' disabled="" value="<?php
print($correo);
?>">
            <label for='correo'>Correo</label>
        </div>
        <div class="input-field col s12">
          <i class="material-icons prefix">mode_edit</i>
          <textarea id="icon_prefix2" class="materialize-textarea" name="residencia" disabled=""><?php
print($residencia);
?></textarea>
          <label for="icon_prefix2">Lugar de Residencia</label>
        </div>
    </div>
    <div class='row center-align'>
         <a class='btn waves-effect black' href="index.php"><i class='material-icons left'>replay</i>Regresar</a>
    </div>
    <br>
</form>

<?php
Page::footer();
?>
