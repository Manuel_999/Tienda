<?php
require("../../lib/conexion.php");
$sql  = "SELECT * FROM usuarios";
$data = Database::getRows($sql, null);

if ($data == null) {
  header("location: registrar.php");
}


if (!empty($_POST)) {
  $alias = htmlspecialchars($_POST['usuario']);
  $clave = htmlspecialchars($_POST['clave']);
  try {
    if ($alias != "" && $clave != "") {
      $sql   = "SELECT * FROM usuarios WHERE usuario = ?";
      $param = array(
        $alias
      );
      $data  = Database::getRow($sql, $param);
      $hash  = $data['clave'];
      if ($data != null) {
        if (password_verify($clave, $hash)) {
          session_start();
          $_SESSION['usuario']       = $data['usuario'];
          $_SESSION['clave']         = $data['clave'];
          $_SESSION['id']            = $data['id_usuario'];
          $_SESSION['tipo']          = $data['id_tipo_usuario'];
          $_SESSION['foto']          = $data['foto'];
          $_SESSION['ultimoingreso'] = date("Y-n-j H:i:s");
          header("location: principal.php");
        } else {
          throw new Exception("La clave ingresada es incorrecta.");
        }
        
      } else {
        throw new Exception("El alias ingresado no existe.");
      }
    } else {
      throw new Exception("Debe ingresar un alias y una clave.");
    }
  }
  catch (Exception $error) {
    print("<div class='card-panel red white-text'><i class='material-icons left'>error</i>" . $error->getMessage() . "</div>");
  }
}

session_start();
if (isset($_SESSION['usuario'])) {
  if ($_SESSION['usuario'] != null) {
    
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<meta charset="utf-8"/>
<link rel="stylesheet" href="../../css/materialize.min3.css">
<link href="css/icons.css" rel="stylesheet">
<link rel='stylesheet' type='text/css' href='../../css/icons.css'>

<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<body>
    <form class="col s12 container" method='post'>
      <h1 class="center-align">Iniciar Sesión</h1>
        <div class="center-align">
          <i class="material-icons large">perm_identity</i>
        </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="usuario" type="email" name="usuario" autocomplete="off">
          <label for="usuario">Usuario</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="clave" type="password" name="clave">
          <label for="clave">Contraseña</label>
        </div>
      </div>
      <label>
         <a class='pink-text' href='#!'><b><h6>¿Has olvidado tu contraseña?</h6></b></a>
      </label>
      <div class="section"></div>
      <center>
              <div class='row'>
                <button type='submit' class='col s12 btn btn-large waves-effect indigo darken-4'>Iniciar</button>
              </div>
            </center>
</form>
  

        <!-- Importamos el JQuery de materilize  -->
        <script src="../../js/jquery.js"></script>
        <script src="../../js/materialize.min.js"></script>
        <script type="text/javascript" src="../../js/main.js"></script>
        <script type="text/javascript">
          $(document).ready(function(){
            $('.slider').slider();
          });
        </script>

</body>
</html>