<?php
require("../../lib/conexion.php");
require("../../lib/validator.php");

$sql  = "SELECT * FROM usuarios";
$data = Database::getRows($sql, null);
if ($data != null) {
  header("location: login.php");
}

if (!empty($_POST)) {
  $_POST     = Validator::validateForm($_POST);
  $nombres   = $_POST['nombres'];
  $apellidos = $_POST['apellidos'];
  $correo    = $_POST['correo'];
  $clave1    = $_POST['clave1'];
  $clave2    = $_POST['clave2'];
  try {
    if ($nombres != "" && $apellidos != "") {
      if ($correo != "") {
        if ($clave1 != "" && $clave2 != "") {
          if ($clave1 == $clave2) {
            $clave  = password_hash($clave1, PASSWORD_DEFAULT);
            $sql    = "INSERT INTO usuarios(nombres, apellidos, usuario, clave, id_tipo_usuario, foto) VALUES(?, ?, ?, ?, 1, ?)";
            $params = array(
              $nombres,
              $apellidos,
              $correo,
              $clave,
              Validator::imagen_usuario()
            );
            Database::executeRow($sql, $params);
            header("location: login.php");
          } else {
            throw new Exception("Las contraseñas no coinciden");
          }
        } else {
          throw new Exception("Debe ingresar ambas contraseñas");
        }
      } else {
        throw new Exception("Debe ingresar un correo electrónico");
      }
    } else {
      throw new Exception("Debe ingresar el nombre completo");
    }
  }
  catch (Exception $error) {
    
  }
}
?>


<!DOCTYPE html>
        <html lang="es">
        <head>
        <title>Inicio | FireStore</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="../../css/materialize.min3.css">
        <link href="css/icons.css" rel="stylesheet">
        <link rel='stylesheet' type='text/css' href='../../css/icons.css'>

        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <nav class="indigo darken-3">
        
    </nav>
  

<form class="container" method='post'>
    <div class="row">
        <h2 class="center-align">Registrar Usuario</h2>
    </div>
    <div class="row">
        <div class="col offset-s4">
            <img src="../../img/usuarios/predeterminado.png">
        </div>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
              <i class='material-icons prefix'>person</i>
              <input id='nombres' type='text' name='nombres' class='validate'>
              <label for='nombres'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellidos' type='text' name='apellidos' class='validate'>
            <label for='apellidos'>Apellidos</label>
        </div>
        <div class='input-field col s12 m12'>
            <i class='material-icons prefix'>email</i>
            <input id='correo' type='email' name='correo' class='validate' >
            <label for='correo'>Correo</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' class='validate' >
            <label for='clave1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate' >
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <div class='row center-align'>
         <button type='submit' class='btn waves-effect red darken-4'>Aceptar<i class='material-icons left'>send</i></button>
    </div>
    <br>
</form>

  <!-- Importamos el JQuery de materilize  -->
        <script src="../../js/jquery.js"></script>
        <script src="../../js/materialize.min.js"></script>
        <script type="text/javascript" src="../../js/main.js"></script>
        <script type="text/javascript">
          $(document).ready(function(){
            $('.slider').slider();
          });
        </script>
        </body>
        </html>