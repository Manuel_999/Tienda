<?php
require("../../lib/conexion.php");
require("../../lib/validator.php");
require("../../lib/Zebra_Pagination.php");
session_start();
class Page
{
  public static function header($titulo)
  {
    //Se verifica que un usuario este logueado primero.
    if ($_SESSION['usuario'] == null && $_SESSION['contraseña'] == null) {
      header("location: ../main/login.php");
    }
    $cuerpo = "<!DOCTYPE html>
                <html>
                <head>
                  <title>$titulo</title>
                </head>
                <meta charset='utf-8'/>
                <link rel='stylesheet' href='../../css/materialize.min3.css'>
                <link href='css/icons.css' rel='stylesheet'>
                <link rel='stylesheet' type='text/css' href='../../css/icons.css'>
                <body>
                  <nav class='indigo darken-4'>
                    <div class='nav-wrapper'>
                      <a href='../main/principal.php' class='brand-logo'><a href='#!' class='brand-logo'><img src='../../img/logo/logo.png' width='60' height='60'></a>
                      <a href='#' data-activates='mobile-demo' class='button-collapse'><i class='material-icons'>menu</i></a>
                      <ul class='right hide-on-med-and-down'>";
    if ($_SESSION['tipo'] == "1") {
      $cuerpo .= "
                        <li><a href='../usuarios/'>Usuarios</a></li>";
    }
    $cuerpo .= "<li><a href='../accesorios/'>Accesorios</a></li>
                        <li><a href='mobile.html'>Pedidos</a></li>
                        <li><a href='../clientes/'>Clientes</a></li>
                        <li><a href='../main/logout.php'>Cerrar sesión</a></li>
                      </ul>
                      <ul class='side-nav' id='mobile-demo'>
                        <li><a href='sass.html'>Sass</a></li>
                        <li><a href='badges.html'>Components</a></li>
                        <li><a href='collapsible.html'>Javascript</a></li>
                        <li><a href='mobile.html'>Mobile</a></li>
                      </ul>
                    </div>
                  </nav>";
    print($cuerpo);
  }
  
  public static function footer()
  {
    $footer = "<!-- Importamos el JQuery de materilize  -->
              <script src='../../js/jquery.js'></script>
              <script src='../../js/materialize.min.js'></script>
              <script type='text/javascript' src='../../js/main.js'></script>
              <script type='text/javascript'>
                $(document).ready(function(){
                  $('.slider').slider();
                });
                $(document).ready(function() {
                  $('select').material_select();
                });
              </script>
      </body>
      </html>";
    print($footer);
  }
  public static function setCombo($label, $name, $value, $query)
  {
    $data = Database::getRows($query, null);
    print("<select name='$name' required>");
    if ($data != null) {
      if ($value == null) {
        print("<option value='' disabled selected>Seleccione una opción</option>");
      }
      foreach ($data as $row) {
        if (isset($_POST[$name]) == $row[0] || $value == $row[0]) {
          print("<option value='$row[0]' selected>$row[1]</option>");
        } else {
          print("<option value='$row[0]'>$row[1]</option>");
        }
      }
    } else {
      print("<option value='' disabled selected>No hay registros</option>");
    }
    print("
      </select>
      <label>$label</label>
    ");
  }
  
}

?>