<ul id="dropdown1" class="dropdown-content">
  <li><a href="../informacion_corporativa/misionv.php">¿Quienes somos?</a></li>
  <li class="divider"></li>
  <li><a href="../informacion_corporativa/equipo.php">Equipo de trabajo</a></li>
</ul>

<ul id="dropdown2" class="dropdown-content">
  <li><a href="../terminos_uso/">Sección de comentarios</a></li>
  <li class="divider"></li>
  <li><a href="../terminos_uso/terminos_uso.php">Terminos de uso</a></li>
</ul>
<nav class="indigo darken-4">
    <div>
      <a href="#!" class="brand-logo"><img src="../../img/logo/logo.png" width="60" height="60"></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="../inicio/index.php">Inicio</a></li>
        <li><a href="../producto/producto.php">Accesorios</a></li>
        <li><a href="../noticias/noticias.php">Noticias</a></li>
        <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Información<i class="material-icons right">arrow_drop_down</i></a></li>
        <li><a class="dropdown-button" href="#!" data-activates="dropdown2">Terminos<i class="material-icons right">arrow_drop_down</i></a></li>
        <li><a href="../preguntas_frecuentes/preguntas.php">Preguntas</a></li>
        <li><a href="../contacto/contactos.php">Contactanos</a></li>
        <?php
session_start();
if (isset($_SESSION['id'])) {
  if ($_SESSION['id'] == null) {
    print("<li><a href='../login/iniciar_sesion.php'>Iniciar sesión</a></li>");
  } else {
    print("<li><a href='../login/logout.php'>Cerrar sesión</a></li>");
  }
} else {
  print("<li><a href='../login/iniciar_sesion.php'>Iniciar sesión</a></li>");
}
?>
     </ul>
    </div>
  </nav>



 