 <!DOCTYPE html>
        <html lang="es">
        <head>
        <title>Inicio | FireStore</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="../../css/materialize.min3.css">
        <link href="css/icons.css" rel="stylesheet">
        <link rel='stylesheet' type='text/css' href='../../css/icons.css'>

        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        </head>
        <body>

     <?php 
         include("../../lib/menu.php"); 
     ?>
      
  <div class="slider">
    <ul class="slides">
      <li>
        <img src="../../img/slider/foto.jpg"> <!-- random image -->
        <div class="caption center-align">
        </div>
      </li>
      <li>
        <img src="../../img/slider/foto2.jpg"> <!-- random image -->
        <div class="caption left-align">
        </div>
      </li>
      <li>
        <img src="../../img/slider/foto3.jpg"> <!-- random image -->
        <div class="caption right-align">
        </div>
      </li>
      <li>
        <img src="../../img/slider/foto4.jpg"> <!-- random image -->
        <div class="caption center-align">
        </div>
      </li>
    </ul>
  </div>

<!--cartas-->
<div class= "row">

 <div class=" wrapper row">
<div class="col s12 m4">
    <h5 class="header">Peinetas</h5>
    <div class="card horizontal">
      <div class="card-image">
        <img src="../../img/card/peine.jpg">

      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Son accesorios para mujer llamativos y vienen con diseños modernos. Las peinetas hacen lucir muy bien el cabello, además se pueden usar de día o de noche</p>
        </div>
        <div class="card-action">
          <a href="#"></a>
        </div>
      </div>
    </div>
    </div>


<div class=" wrapper row">
<div class="col s12 m4">
    <h5 class="header">Cinturones</h5>
    <div class="card horizontal">
      <div class="card-image">
        <img src="../../img/card/info2.jpg">

      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Es uno de los accesorios para mujer más usado. Podemos encontrar cinturones anchos que le dan volumen a la cintura o delgados que hacen ver la figura mas hermosa</p>
        </div>
        <div class="card-action">
          <a href="#"></a>
        </div>
      </div>
    </div>
    </div>
<div class=" wrapper row">
<div class="col s12 m4">
    <h5 class="header">Aretes</h5>
    <div class="card horizontal">
      <div class="card-image">
        <img src="../../img/card/info3.jpg">

      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Son accesorios para mujer que siempre las acompañan. Si el cuello es corto los aretes deben ser cortos y si es largo los aretes también deben serlo.</p>
        </div>
        <div class="card-action">
          <a href="#"></a>
        </div>
      </div>
    </div>
    </div>


    </div> 
    </div>
    </div>
    </div>


        
                
    <?php 
         include("../../lib/footer.php"); 
     ?>
 <!-- apis me gusta-->
 <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.9";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        <!-- Importamos el JQuery de materilize  -->
        <script src="../../js/jquery.js"></script>
        <script src="../../js/materialize.min.js"></script>
        <script type="text/javascript" src="../../js/main.js"></script>
        <script type="text/javascript">
          $(document).ready(function(){
            $('.slider').slider();
          });
        </script>
        </body>
        </html>
      
      

        