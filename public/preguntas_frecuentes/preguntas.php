 <!DOCTYPE html>
        <html lang="es">
        <head>
        <title>Preguntas frecuentes | FireStore</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="../../css/materialize.min3.css">
        <link href="../../css/icons.css" rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        </head>
        <body>
        <?php 
         include("../../lib/menu.php"); 
     ?>

      <div class="container">
            <div class="section"></div>
            <h4>Preguntas frecuentes:</h4>
            <ul class="collapsible" data-collapsible="accordion">
              <li>
                <div class="collapsible-header"><i class="material-icons">place</i>¿Como funciona el envio de las prendas en fire store?</div>
                <div class="collapsible-body"><p>La entrega es a domicilio, usted como nuestro cliente simplemente debe de notificarnos un punto de encuentro.</p></div>
              </li>
            </ul>
            <ul class="collapsible" data-collapsible="accordion">
              <li>
                <div class="collapsible-header"><i class="material-icons">place</i>¿Como es la forma de pago?</div>
                <div class="collapsible-body"><p>La forma de pago es a contra entrega, usted como nuestro cliente previamente debe noticarnos su pedido y nosotros le programamos su horario de entrega.</p></div>
              </li>
            </ul>
            <ul class="collapsible" data-collapsible="accordion">
              <li>
                <div class="collapsible-header"><i class="material-icons">place</i>¿Horarios de atencion?</div>
                <div class="collapsible-body"><p>Puedes escribirnos de lunes a domingo y hacer tu pedido, dias habiles de entrega, de lunes a sabado de 8 am a 3pm.</p></div>
              </li>
            </ul>
            <ul class="collapsible" data-collapsible="accordion">
              <li>
                <div class="collapsible-header"><i class="material-icons">place</i>¿Como realizo mi pedido?</div>
                <div class="collapsible-body"><p>Simplemente crea tu cuenta en la pagina oficial de fire store, al momento de agregar prendas a tu carrito debes de ingresar tu usuario y contraseña, finalmente nosotros te notificamos, cuando sera la entrega de tus prendas y rl costo final.</p></div>
              </li>
            </ul>
            <div class="section"></div>
      </div>

        <?php 
         include("../../lib/footer.php"); 
     ?>
      
        <!-- Importamos el JQuery de materilize  -->
        <script src="../../js/jquery.js"></script>
        <script src="../../js/materialize.min.js"></script>
        <script type="text/javascript" src="../../js/main.js"></script>
        </body>
        </html>

        