<!DOCTYPE html>
        <html lang="es">
        <head>
        <title>Contactos | FireStore</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="../../css/materialize.min3.css">
        <link href="../../css/icons.css" rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        </head>
        <body>

       <?php 
         include("../../lib/menu.php"); 
     ?>

     <div class="section"></div>
     <div class="container">
       <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15505.100417030288!2d-89.2067646196046!3d13.701780945523671!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2ssv!4v1488142140139" width="600" height="600" frameborder="0" style="border:0; width: 100%; height: 400px;" allowfullscreen></iframe>
      
        <div class="row">
          <div class="col s12 m12">
            <div class="card blue-grey darken-1">
              <div class="card-content white-text">
                <span class="card-title">Direccion</span>
                <p>Estamos ubicados en Boulevard De Los Héroes, San Salvador centro comercial metrocentro local 65 frente a pollo campero.  </p>
              </div>
              <div class="card-action">
                <a href="#"></a>
                <a href="#"></a>
              </div>
            </div>
          </div>
        </div>

   <div class="row">
          <div class="col s12 m6">
            <div class="card">
              <div class="card-image">
                <img src="../../img/logo/logo3.png" style="width: 370px;margin-left: 40px;">
              </div>
              <div class="card-content">
                <p>Siguenos en nuestra pagina de facebook y enterate de todas nuestras promosiones y ofertas, demas de nuestras nuevas prendas. Link a continuacion:</p>
              </div>
              <div class="card-action">
                <a href="https://www.facebook.com/FireStore-1341030652643452/?ref=settings">Fire Store El Salvador</a>
              </div>
            </div>
          </div>
          <div class="col s12 m6">
            <div class="card">
              <div class="card-image">
                <img src="../../img/logo/logo2.jpg">
              </div>
              <div class="card-content">
                <p>Escribenos a nuestro correo electronico firestore1999@gmail.com</p>
              </div>
              <div class="card-action">
                <a href="https://mail.google.com">Fire Store El Salvador</a>
              </div>
            </div>
          </div>
        </div>
  
     </div>

     <?php 
         include("../../lib/footer.php"); 
     ?>

        <!-- Importamos el JQuery de materilize  -->
        <script src="../../js/jquery.js"></script>
        <script src="../../js/materialize.min.js"></script>
        <script type="text/javascript" src="../../js/main.js"></script>
        </body>
        </html>