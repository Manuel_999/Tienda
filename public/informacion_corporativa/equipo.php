 <!DOCTYPE html>
        <html lang="es">
        <head>
        <title>Equipo de trabajo | FireStore</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="../../css/materialize.min3.css">
        <link href="../../css/icons.css" rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        </head>
        <body>

              <?php 
         include("../../lib/menu.php"); 
     ?>        



<div class="carousel">
    <a class="carousel-item" href="#one!"><img src="../../img/equipo/integrante1.jpg"></a>
    <a class="carousel-item" href="#two!"><img src="../../img/equipo/integrante2.jpg"></a>
  </div>

<div class="container row">
  <h2 class="center-align">Valores</h2>
  <hr>
  <ol>
    <li>Puntualidad: en este caso se hace referencia a este valor para exigir a los empleados el respeto de los tiempos de llegada y salida, pero sobre todo para con los clientes, por ejemplos a la hora de presentar proyectos o realizar entregas.</li>
    <li>Calidad: en este caso se intenta que los productos o servicios ofrecidos sean de excelencia.</li>
    <li>Consecuencia: hace referencia a la coherencia que deben tener los empleadores para con sus trabajadores, como de la empresa con los clientes. En caso de haber compromisos deben ser cumplidos.</li>
    <li>Justicia: este concepto tiene una mayor orientación para sus trabajadores. Se hace referencia a otorgar a cada uno lo que le corresponde, no solo desde el punto de vista salarial sino que también en cuanto se refiere a las actividades que a cada uno le tocará desempeñar.</li>
    <li>Comunicación: en tanto se toma la comunicación como un valor fundamental se intenta que las relaciones y conexiones dentro de los miembros de la empresa y con los clientes sea fluida y sincera.</li>
    <li>Responsabilidad: tiene varias orientaciones. Por ejemplo si se hace referencia a los trabajadores, la empresa se compromete a la estabilidad y buenas condiciones laborales. En cuanto a los clientes, la empresa se compromete a entregar bienes y servicios de calidad. Algo que también resulta muy importante hoy en día es el compromiso con el medio ambiente. Para ello es necesario cumplir con las leyes determinadas e incluso exceder las mismas para continuar con su preservación.</li>
    <li>Originalidad: refiere a las innovaciones, cambios y creaciones tanto en los bienes y servicios, en las metodologías laborales y estrategias.</li>
    <li>Seguridad: este juicio se orienta a generar un vínculo de confianza, que los clientes crean que serán satisfechos en sus necesidades y deseos.</li>
    <li>Libertad: en este caso se intenta que tanto los empleados y los clientes puedan expresarse con total seguridad en caso de tener creencias u opiniones distintas, siempre que sean presentadas con respeto y cordialidad.</li>
    <li>Trabajo en equipo: desde éste se intenta la integración de cada uno de miembros de la empresa al grupo laboral, que sean promovidos mejores resultados gracias a un ambiente positivo. Para ello es elemental la participación de los distintos miembros de la empresa en diversos ámbitos.</li>
    <li>Honestidad: orientado tanto para los miembros de la empresa entre sí, como con los clientes. Se promueve la verdad como una herramienta elemental para generar confianza y la credibilidad de la empresa.</li>

  </ol>
</div>



    <?php 
         include("../../lib/footer.php"); 
     ?>
 




        <!-- Importamos el JQuery de materilize  -->
        <script src="../../js/jquery.js"></script>
        <script src="../../js/materialize.min.js"></script>
        <script type="text/javascript" src="../../js/main.js"></script>
        </body>
        </html>
      
      