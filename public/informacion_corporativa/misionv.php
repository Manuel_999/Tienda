<!DOCTYPE html>
        <html lang="es">
        <head>
        <title>Información corporativa| FireStore</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="../../css/materialize.min3.css">
        <link href="../../css/icons.css" rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        </head>
        <body>
         
               <?php 
         include("../../lib/menu.php"); 
     ?>

<div class="section"></div>
<div class="container">
    <div class="row" style="text-align: justify;">
        <div class="col s12 m4">
            <img src="../../img/misionv/quienes-somos.png"><br><br>
            <h5 class="center-align">¿Quienes somos?</h5>
            <p>Somos una empresa que se dedica a la venta de productos como ropa zalzado y accesorios para mujer y hombres.</p>
        </div>
        <div class="col s12 m4">
            <img src="../../img/misionv/mision.png" style="width: 300px;height: 300px;">
            <h5 class="center-align">Misión</h5>
            <p>En firestore tenemos una vision amplea, queremos complacer a nuestros clientes y llevarles siempre las mejores prendas de temporada que se acomodan a cada estilo, facilitando la forma de pago ya que esta misma es a contra entrega, buscando siempre ofreser productos de las mejores marcas .</p>
        </div>
        <div class="col s12 m4">
            <img src="../../img/misionv/vision.png" style="width: 350px;height: 300px;">
            <h5 class="center-align">Visión</h5>
            <p>Somos una empresa dedicada a la venta de ropa en linea, con el fin de facilitarle a nuestros clientes las mejores marcas a precios economicos y de manera online </p>
        </div>
    </div>    
</div>


   
    <?php 
         include("../../lib/footer.php"); 
     ?>
   

         </body>
        </html>
 <!-- Importamos el JQuery de materilize  -->
        <script src="../../js/jquery.js"></script>
        <script src="../../js/materialize.min.js"></script>
        <script type="text/javascript" src="../../js/main.js"></script>
        </body>
        </html>