<!DOCTYPE html>
        <html lang="es">
        <head>
        <title>Noticias | FireStore</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="../../css/materialize.min3.css">
        <link href="../../css/icons.css" rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        </head>
        <body>

       <?php 
         include("../../lib/menu.php"); 
     ?>

      <!--cartas-->
<div class= "row">

 <div class=" wrapper row">
<div class="col s12 m4">
    <h5 class="header">LA LEYENDA DE LOS AHORAS</h5>
    <div class="card horizontal">
      <div class="card-image">
        <img src="../../img/noticias/moda.jpg" style="width: 340px;margin-left: 5px;">

      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Óscar Soria (escritor) y Chabela Quintanilla (diseñadora gráfica y videógrafa)
son los responsables de Los Ahoras,</p>
        </div>
        <div class="card-action">
          <a href="#"></a>
        </div>
      </div>
    </div>
    </div>


<div class=" wrapper row">
<div class="col s12 m4">
    <h5 class="header">Joyas con savoir-faire español</h5>
    <div class="card horizontal">
      <div class="card-image">
        <img src="../../img/noticias/moda1.jpg" style="width: 340px;margin-left: 5px;">

      </div>
      <div class="card-stacked">
        <div class="card-content">
     <p class ="style=”text-align: justify";>Fabricación nacional y diseños con personalidad
              Así son las joyas de autor de estas jóvenes
              firmas que están conquistando el sector.
</p>
        </div>
        <div class="card-action">
          <a href="#"></a>
        </div>
      </div>
    </div>
    </div>
<div class=" wrapper row">
<div class="col s12 m4">
    <h5 class="header">EL "HEARTMADE" DE LECARRÉ</h5>
    <div class="card horizontal">
      <div class="card-image">
        <img src="../../img/noticias/modis3.jpg" style="width: 340px; margin-left:5px;">

      </div>
      <div class="card-stacked">
        <div class="card-content">
          <p>Con más de 30 años de experiencia en la fabricación y distribución de joyas,
Le Carré forma parte de un grupo empresarial 
.</p>
        </div>
        <div class="card-action">
          <a href="#"></a>
        </div>
      </div>
    </div>
    </div>
<!--video-->
    <iframe width="1350" height="500" src="https://www.youtube.com/embed/VEteFCdce4k"  allowfullscreen></iframe>
    </div> 
    </div>
    </div>
    </div>

            

        <?php 
         include("../../lib/footer.php"); 
     ?>
        <!-- Importamos el JQuery de materilize  -->
        <script src="../../js/jquery.js"></script>
        <script src="../../js/materialize.min.js"></script>
        <script type="text/javascript" src="../../js/main.js"></script>
        </body>
        </html>