<!DOCTYPE html>
        <html lang="es">
        <head>
        <title>Accesorios | FireStore</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="../../css/materialize.min3.css">
        <link href="../../css/icons.css" rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        </head>
        <body>

     <?php 
         include("../../lib/menu.php"); 
     ?>     

<div class="section"></div>


      <div class="row">

        <?php
          require("../../lib/conexion.php");
          require("../../lib/validator.php");
          $sql = "SELECT * FROM accesorios, tipo_accesorio WHERE tipo_accesorio.id_tipo_accesorio = accesorios.id_tipo_accesorio";
          $params = null;
          $data = Database::getRows($sql, $params);
          foreach($data as $row)
          {
            if ($row['estado_visible'] == 2) {
              print("<div class='col s12 m6 l4 offset-l1'>
                      <div class='card'>
                        <div class='card-image'>
                          <img src='data:image/*;base64,".$row['foto']."' class='materialboxed' width='100' height='300'>
                        </div>
                        <div class='card-content'>
                          <p>Titulo: ".$row['titulo']."</p>
                          <p>Descripción: ".$row['descripcion']."</p>
                          <p>Tipo: ".$row['tipo_accesorio']."</p>
                          <p>Precio: $".$row['precio']."</p>
                          <p>Existencia: ".$row['existencias']."</p>
                        </div>
                        <div class='card-action'>");
                        if ($row['existencias'] > 0) {
                          if (isset($_SESSION['id'])) {
                            print("<a href='pedido.php?id=".$row['id_accesorios']."&&id2=".$_SESSION['id']."' class='btn blue accent-4'><i class='material-icons left'>shop</i>Agregar al carrito </a>");
                          }else{
                            print("<a href='../login/iniciar_sesion.php' class='btn blue accent-4'><i class='material-icons left'>shop</i>Agregar al carrito </a>");
                          }
                        }else{
                          print("<a href='#' class='btn blue accent-4' disabled><i class='material-icons left'>shop</i>Agregar al carrito </a>");
                        }
                        
                        print("</div>
                      </div>
                    </div>");
            }
            
          }
        ?>
      </div> 


                  
    <?php 
         include("../../lib/footer.php"); 
     ?>
 
        <!-- Importamos el JQuery de materilize  -->
        <script src="../../js/jquery.js"></script>
        <script src="../../js/materialize.min.js"></script>
        <script type="text/javascript" src="../../js/main.js"></script>
        </body>
        </html>