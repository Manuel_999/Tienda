<?php
  require("../../lib/conexion.php");

  if(!empty($_POST))
  {
      $alias = htmlspecialchars($_POST['usuario']);
      $clave = htmlspecialchars($_POST['clave']);
      try
      {
        if($alias != "" && $clave != "")
        {
            $sql = "SELECT * FROM clientes WHERE correo_electronico = ?";
            $param = array($alias);
            $data = Database::getRow($sql, $param);
            $hash = $data['clave'];
            if($data != null)
            {
               if(password_verify($clave, $hash)) 
               {      
                    session_start();
                    $_SESSION['id'] = $data['id_cliente'];
                    header("location: ../index.php");
               }else{
                   throw new Exception("La clave ingresada es incorrecta.");
                }
          
            }else{
              throw new Exception("El alias ingresado no existe.");
            }
        }else{
          throw new Exception("Debe ingresar un alias y una clave.");
        }
      }
      catch (Exception $error)
      {
          print("<div class='card-panel red white-text'><i class='material-icons left'>error</i>".$error->getMessage()."</div>");
      }
  }
?>

<!DOCTYPE html>
        <html lang="es">
        <head>
        <title>Iniciar Sesion | FireStore</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="../../css/materialize.min3.css">
        <link href="../../css/icons.css" rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        </head>
        <body>

<ul id="dropdown1" class="dropdown-content">
  <li><a href="../informacion_corporativa/misionv.php">¿Quienes somos?</a></li>
  <li class="divider"></li>
  <li><a href="../informacion_corporativa/equipo.php">Equipo de trabajo</a></li>
</ul>

<ul id="dropdown2" class="dropdown-content">
  <li><a href="../terminos_uso/">Sección de comentarios</a></li>
  <li class="divider"></li>
  <li><a href="../terminos_uso/terminos_uso.php">Terminos de uso</a></li>
</ul>
<nav class="indigo darken-4">
    <div>
      <a href="#!" class="brand-logo">FireStore</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="../inicio/index.php">Inicio</a></li>
        <li><a href="../producto/producto.php">Accesorios</a></li>
        <li><a href="../noticias/noticias.php">Noticias</a></li>
        <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Información<i class="material-icons right">arrow_drop_down</i></a></li>
        <li><a class="dropdown-button" href="#!" data-activates="dropdown2">Terminos<i class="material-icons right">arrow_drop_down</i></a></li>
        <li><a href="../preguntas_frecuentes/preguntas.php">Preguntas</a></li>
        <li><a href="../contacto/contactos.php">Contactanos</a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="sass.html">Sass</a></li>
        <li><a href="badges.html">Components</a></li>
        <li><a href="collapsible.html">Javascript</a></li>
        <li><a href="mobile.html">Mobile</a></li>
      </ul>
    </div>
  </nav>

<br>
    <form method="post">
    <div class="row">
        <div class="col s6 offset-s3">
          <div class="row">
            <div class="col s12 center-align">
              <h3>Iniciar Sesión</h3>
            </div>        
          </div>
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">account_circle</i>
              <input id="icon_prefix" type="text" class="validate" name="usuario" autocomplete="off">
              <label for="icon_prefix">Usuario</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">security</i>
              <input id="icon_telephone" type="password" class="validate" name="clave">
              <label for="icon_telephone">Contraseña</label>
            </div>
          </div>
          <div class="row">
            <div class="col s12 center-align">
              <button class="waves-effect waves-light btn red darken-4" type="submit">Iniciar Sesion </button>
              <h5>ó</h5>
              <a href="registrar.php">Registrarse</a>
            </div>        
          </div>
        </div>      
    </div>
      
    </form>
<br>
     <?php 
         include("../../lib/footer.php"); 
     ?>
        <!-- Importamos el JQuery de materilize  -->
        <script src="../../js/jquery.js"></script>
        <script src="../../js/materialize.min.js"></script>
        <script type="text/javascript" src="../../js/main.js"></script>
        </body>
        </html>